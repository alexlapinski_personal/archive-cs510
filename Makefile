.PHONY: clean run test
all: clean run
clean:
	rm -rf *.pyc

run:
	@echo "See Readme.md file for detailed instructions on running this solution"
	@echo
	python -m othello.runner

test:
	python -m unittest -v tests.montecarlo.searchnode_tests tests.othello_generate_moves_tests
