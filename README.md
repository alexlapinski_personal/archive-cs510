# CS510 - HW3 - Part 1 - Alex Lapinski

## How to Run
Execute ```python -m othello.runner <player1> <player2> <stats>```
player 1 / player 2 can be any of the following
 * ```random``` - The random move player
 * ```minimax:N``` - The minimax player where N is max depth to explore
 * ```montecarlo:N``` - The montecarlo player where N is the number of iterations

## Results
I've included a mechanism to write out game results to a csv file. The result of my own tests are included in the results, and any new run of the python module will append the result to this csv.

My own testing for completeness included the following scenarios:
 * Random vs Random
 * Random vs Minimax (varying depths)
 * Minimax vs Random (varying depths)
 * Random vs MonteCarlo (varying iterations)
 * MonteCarlo vs Random (varying iterations)
 * Minimax vs MonteCarlo (varying depths and iterations)
 * MonteCarlo vs Minimax (varying depths and iterations)

The results are as follows:
### Random vs Random
I ran 100 games, and found that scores were more or less random across all games (as expected). The majority of these scores ~20 seemed to be centered arond +/-10

### Random vs Minimax
I ran both scenarios where Minimax was player1 and where minimax was player2. Both times I varied the max depth for minimax between 1 and 4.

The results were such that the deeper minimax was able to explore, the more often the minimax player beat the random player.

At a max depth of 4, the average execution time was around 80 seconds.

 * Depth 1 - Won 75% of the time
 * Depth 2 - Won 80% of the time
 * Depth 3 - Won 90% of the time
 * Depth 4 - Won 90% of the time

Using a depth of 5 takes around 12 minutes to execute against random player.

### Random vs MonteCarlo Tree Search
I ran both scenarios where MCTS was player 1 and player 2 with varying number of iterations (1, 100, 1000, 10000).

The results seemed to indicate that the sweet spot for # of iterations was between 100 and 1000, where 10000 iterations did not actualy improve the play against the random player.

The positive benefit is that at around 1000 iterations the execution time was still only averaging around 2 to 3 seconds. Dramatically faster than for Minimax at max depth of 4.

 * 1 Iteration - Won 40% of the time
 * 100 Iterations - Won 90% of the time
 * 1000 Iterations - Won 85% of the time
 * 10000 Iterations - Won 75% of the time

 The python code I have written does allow for iterations over 10000, I have tried with 50000 with an average execution time of 2mins and 100000 in around 2.5 mins and 1,000,000 iterations in roughly 24mins.

### Minimax vs MonteCarlo Tree Search
Similar to the tests above, I ran Minimax and MCTS with varying iterations and max depths against each other. Below are the win/loss results

 * MCTS:1 vs Minimax:1 - Minimax won 100% of the time
 * MCTS:1 vs Minimax:2 - Minimax won 100% of the time
 * MCTS:1 vs Minimax:3 - MCTS won 100% of the time
 * MCTS:1 vs Minimax:4 - MCTS won 100% of the time
 * MCTS:100 vs Minimax:1 - MCTS won 70% of the time, Minimax 30%
 * MCTS:100 vs Minimax:2 - MCTS won 20% of the time, Minimax 80%
 * MCTS:100 vs Minimax:3 - MCTS won 30% of the time, Minimax 70%
 * MCTS:100 vs Minimax:4 - MCTS won 50% of the time, Minimax 50%
 * MCTS:1000 vs Minimax:1 - MCTS won 70% of the time, Minimax 30%
 * MCTS:1000 vs Minimax:2 - MCTS won 40% of the time, Minimax 50%, 10% tie
 * MCTS:1000 vs Minimax:3 - MCTS won 40% of the time, Minimax 60%

The case where there was just 1 iteration of MCTS the results are inconclusive.
The case with MCTS has 100 iterations, we see it dominates minimax only at depth 1
The case where MCTS has 1000 iterations, we see more wins, but minimax still performs better.

The outcome of these tests is that minimax wins more often, but takes more time to execute it's search of states when searching at a reasonable depth (e.g. 4).

### Results
The tuning of Minimax lead me to belive that the best tradeoff between execution time and success against random player was a max depth of 4.

The tuning of MCTS lead me to believe that the best tradeoff for percentage of wins aginst random player was between 100 and 1000 iterations.

The real benefit of MCTS is the drasticaly smaller amount of time needed to run to reach the same competenence and percentage of wins. about 40 times faster. Even when I increased the iterations for MCTS to 10000, the average run time was only 20 seconds. Therefore it seems the number of iterations linearly increases the time needed. Where a depth increase for minimax from 2 to 4 indicated around 100 times increase in time needed.

Further results can be found in games.csv and loaded into excel or google docs.
An example of running mcts vs minimax (best depth & number of iterations) can be found in output-part1.txt

## Cleanup
Execute ```make clean``` to remove all pyc files

## "Othello Java"
Since I am making use of python for all homework assignments instead of java, I have ported the java packge into python. These are located within the othello python package (along with the custom players).

Names of functions have been converted to snake case to be closer to that in python (only for those methods converted from java).
