class OthelloGame():

    PLAYER_NAMES = ("O", "X")
    NOTHING = -1
    PLAYER1 = 0
    PLAYER2 = 1

    @classmethod
    def player_name(cls, player_index):
        """Returns the string representation of the player's name"""
        return cls.PLAYER_NAMES[player_index]

    @classmethod
    def other_player(cls, player):
        """
            Returns the other player, given the current player
        """
        if player == cls.PLAYER1:
            return cls.PLAYER2
        return cls.PLAYER1
