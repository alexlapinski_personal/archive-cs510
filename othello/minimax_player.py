import sys
from player import OthelloPlayer
from game import OthelloGame


class OthelloMinimaxPlayer(OthelloPlayer):

    def __init__(self, player, max_depth):
        self.__player = player
        self.__max_depth = max_depth

    def get_move(self, state):
        # The way the game is scored,
        # If we are player 0, then the higher (positive) number
        # the score produces, then we win
        # However, if we are player 1, then the lower the score
        # the better and we win
        # Therefore, we have to change how we call min/max
        # depending on what player we are

        actions = state.generate_moves()
        action = None

        if(self.__player == OthelloGame.PLAYER1):
            value = -sys.maxint - 1
            for a in actions:
                s = state.apply_move_cloning(a)
                local_value = self.min_value(s, self.__max_depth)
                if local_value > value:
                    value = local_value
                    action = a
        else:
            value = sys.maxint
            for a in actions:
                s = state.apply_move_cloning(a)
                local_value = self.max_value(s, self.__max_depth)
                if local_value < value:
                    value = local_value
                    action = a

        # action is the one that is the max from above
        return action

    def max_value(self, state, depth):
        if depth == 0 or state.game_over():
            return state.score()

        value = -sys.maxint - 1
        moves = state.generate_moves()
        successors = (state.apply_move_cloning(move) for move in moves)
        min_value = self.min_value
        for successor in successors:
            value = max(value, min_value(successor, depth - 1))
        return value

    def min_value(self, state, depth):
        if depth == 0 or state.game_over():
            return state.score()

        value = sys.maxint
        moves = state.generate_moves()
        successors = (state.apply_move_cloning(move) for move in moves)
        max_value = self.max_value
        for successor in successors:
            value = min(value, max_value(successor, depth - 1))
        return value
