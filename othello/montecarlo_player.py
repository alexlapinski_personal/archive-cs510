import sys
import random
from player import OthelloPlayer
from random_player import RandomAgent
from game import OthelloGame


class OthelloMonteCarloPlayer(OthelloPlayer):

    def __init__(self, playerNum, iterations):
        self.__playerNum = playerNum
        self.__agent = MonteCarloTreeSearch(iterations)

    def get_move(self, state):
        return self.__agent.decision(state)


class SearchNode():
    def __init__(self, state, action=None, parent=None):
        self.state = state
        self.action = action
        self.parent = parent
        self.children = []
        self.times_visited = 0
        self.average_score = 0
        self.total_score = 0

    def moves_not_in_children(self):
        """
            Returns a list of moves that are not currently
            present in the children nodes.
        """

        # Get the list of possible moves
        moves = self.state.generate_moves()

        # if we have no children, just return the moves
        if not self.children:
            return moves

        children_moves = dict([(repr(child.action), child.action) for child in self.children])

        # Find the moves that are not in the list of children's actions
        result = []
        append = result.append
        for move in moves:
            if repr(move) not in children_moves:
                append(move)

        return result

    def add_child(self, move):
        """
            Generate a child and attach it to the tree.
            Returns the child
        """
        child_state = None
        if move:
            child_state = self.state.apply_move_cloning(move)
        else:
            child_state = self.state.clone()

        child = SearchNode(child_state, move, self)
        self.children.append(child)
        return child


class MonteCarloTreeSearch():

    def __init__(self, iterations):
        self.iterations = iterations
        self.randomAgent = RandomAgent()

    def decision(self, state):
        root = self.createNode(state)
        for i in xrange(0, self.iterations):
            node = self.treePolicy(root)
            if node:
                node2 = self.defaultPolicy(node)
                node2Score = self.score(node2)
                self.backup(node, node2Score)

        return self.bestChild(root).action

    def createNode(self, state):
        return SearchNode(state)

    def bestChild(self, node):
        """
            Return the best child given a node.
            The node's state's next player determines whether to
            find the min or max average scored child
        """
        best_child = node
        if node.state.next_player == OthelloGame.PLAYER1:
            # Find Child with Maximum Average Score
            max_avg_score = -sys.maxint - 1
            for child in node.children:
                if child.average_score > max_avg_score:
                    max_avg_score = child.average_score
                    best_child = child

        if node.state.next_player == OthelloGame.PLAYER2:
            # Find child with Minimum Average Score
            min_avg_score = sys.maxint
            for child in node.children:
                if child.average_score < min_avg_score:
                    min_avg_score = child.average_score
                    best_child = child

        return best_child

    def treePolicy(self, node):
        """

        """
        moves_not_in_children = node.moves_not_in_children()
        possible_moves = node.state.generate_moves()
        if moves_not_in_children:
            return node.add_child(moves_not_in_children.pop(0))
        elif not possible_moves and not node.state.game_over():
            return node.add_child(None)
        elif node.state.game_over():
            return node

        # All of the moves for this state are in the tree
        # we are not in a terminal node
        policy_rand = random.random()

        node_temp = None

        # 90% of the time, bick the best child node (between 1.0 and 0.1)
        if policy_rand > 0.1:
            node_temp = self.bestChild(node)
        else:  # 10% of the time, pick a child at random (between 0.0 and 0.1)
            node_temp = random.choice(node.children)

        return self.treePolicy(node_temp)

    def defaultPolicy(self, node):
        """
            Select a move at random and create the appropriate
            child node
        """
        current_state = node.state
        while not current_state.game_over():
            moves = current_state.generate_moves()
            move = None
            if moves:
                move = random.choice(moves)

            current_state.apply_move(move)

        return SearchNode(current_state, None, node)

    def score(self, node):
        """
            Returns the score of the game
        """
        return node.state.score()

    def backup(self, node, score):
        """
            propogate the score and number of times this node
            was visited back up the tree
        """
        node.times_visited += 1
        node.total_score += score
        node.average_score = node.total_score / node.times_visited

        if node.parent is not None:
            self.backup(node.parent, score)
