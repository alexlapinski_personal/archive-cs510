from game import OthelloGame


class OthelloMove():

    def __init__(self, player, x, y):
        self.x = x
        self.y = y
        self.player = player

    def __str__(self):
        player_name = OthelloGame.player_name(self.player)
        return "Player {0} to {1}, {2}".format(player_name, self.x, self.y)

    def __repr__(self):
        return "OthelloMove({0}, {1}, {2})".format(self.player, self.x, self.y)
