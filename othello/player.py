from abc import ABCMeta, abstractmethod


class OthelloPlayer:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_move(self, state):
        """Returns an instance of OthelloMove"""
        pass
