import random
from player import OthelloPlayer


class OthelloRandomPlayer(OthelloPlayer):

    def __init__(self):
        self.agent = RandomAgent()

    def get_move(self, state):
        return self.agent.pick_random_move(state)


class RandomAgent:

    def pick_random_move(self, state):
        moves = state.generate_moves()
        if not moves:
            return None

        random_move_index = random.randint(0, len(moves)-1)
        return moves[random_move_index]
