import sys
import csv
import time
import cProfile
import pstats
import os.path
import StringIO
from game import OthelloGame
from state import OthelloState
from random_player import OthelloRandomPlayer
from minimax_player import OthelloMinimaxPlayer
from montecarlo_player import OthelloMonteCarloPlayer


class Recorder:

    def __init__(self, filename):
        self.filename = filename

        # Write the headers if we don't have the file yet
        if not os.path.isfile(self.filename):
            with open(self.filename, "w") as csvfile:
                writer = csv.writer(csvfile, delimiter=",")
                writer.writerow(["Player1", "Player2", "Score", "Time Elapsed"])

    def record_game(self, player1, player2, score, time_elapsed):
        with open(self.filename, "a") as csvfile:
            writer = csv.writer(csvfile, delimiter=",")
            writer.writerow([player1, player2, score, time_elapsed])


class OthelloRunner:

    @classmethod
    def create_player(cls, playerName, playerNum):
        if playerName.startswith("minimax"):
            max_depth = int(playerName.split(":")[-1])
            print "Adding Minimax Player (max_depth: {0})".format(max_depth)
            return OthelloMinimaxPlayer(playerNum, max_depth)
        elif playerName.startswith("montecarlo"):
            n = int(playerName.split(":")[-1])
            print "Adding Montecarlo Player (iterations: {0})".format(n)
            return OthelloMonteCarloPlayer(playerNum, n)
        else:
            print "Adding Random Player"
            return OthelloRandomPlayer()

    @classmethod
    def run(cls, player1, player2, showStats=False, profile=False):
        # Create the game state with the initial position
        # for an 8x8 board
        state = OthelloState(8)
        players = list()

        players.append(cls.create_player(player1, OthelloGame.PLAYER1))
        players.append(cls.create_player(player2, OthelloGame.PLAYER2))

        start_time = time.clock()

        # Display the current state in the console:
        player_name = OthelloGame.player_name(state.next_player)
        print "\nCurrent state, {0} to move".format(player_name)
        print state

        # Get the move from the player:
        move = players[state.next_player].get_move(state)
        print move
        state = state.apply_move_cloning(move)

        while not state.game_over():
            # Display the current state in the console:
            player_name = OthelloGame.player_name(state.next_player)
            print "\nCurrent state, {0} to move".format(player_name)
            print state

            # Get the move from the player:
            move = players[state.next_player].get_move(state)
            print move
            state = state.apply_move_cloning(move)

        # Show the result of the game:
        print "\nFinal state with score: {0}".format(state.score())
        print state

        end_time = time.clock()
        diff = end_time - start_time

        recorder = Recorder("games.csv")
        recorder.record_game(player1, player2, state.score(), diff)

        if showStats:
            if diff < 1.0:
                print "Time Elapsed {0}ms".format(diff * 1000)
            else:
                print "Time Elapsed {0}sec".format(diff)


def print_usage():
    print "Usage is as follows"
    print "python -m othello.runner <player1Type> <player2Type> <stats> <profile>"
    print ""
    print "    player1Type and player2 can be one of the following"
    print "       - 'minimax:<depth>' (where <depth> is the max depth to search"
    print "       - 'montecarlo:<iterations>' (where <iterations> is the number of iterations in the sample"
    print "       - 'random'"
    print
    print "stats and profile are optional, and display both stats and profiling information"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print_usage()
        exit
    else:
        player1 = sys.argv[1]
        player2 = sys.argv[2]

        showStats = ("stats" in sys.argv)
        profile = ("profile" in sys.argv)

        if profile:
            pr = cProfile.Profile()
            pr.enable()

        OthelloRunner.run(player1, player2, showStats, profile)

        if profile:
            pr.disable()
            s = StringIO.StringIO()
            ps = pstats.Stats(pr, stream=s).sort_stats("cumulative")
            ps.print_stats()
            ps.print_callers()
            print s.getvalue()
