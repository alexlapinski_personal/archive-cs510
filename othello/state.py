from move import OthelloMove
from game import OthelloGame


class OthelloState():

    def __init__(self, board_size):
        self.next_player = OthelloGame.PLAYER1
        self.board_size = board_size
        self.player_moves = {OthelloGame.PLAYER1: None, OthelloGame.PLAYER2: None}

        if self.board_size < 2:
            self.board_size = 2

        self.board = [[OthelloGame.NOTHING for x in xrange(self.board_size)] for y in range(self.board_size)]

        midpoint = self.board_size / 2
        self.board[midpoint-1][midpoint-1] = OthelloGame.PLAYER1
        self.board[midpoint][midpoint] = OthelloGame.PLAYER1
        self.board[midpoint-1][midpoint] = OthelloGame.PLAYER2
        self.board[midpoint][midpoint-1] = OthelloGame.PLAYER2

    def __str__(self):
        output = list()

        for j in xrange(self.board_size):
            for i in xrange(self.board_size):
                cell = self.board[i][j]
                if cell == OthelloGame.NOTHING:
                    output.append(".")
                else:
                    output.append(OthelloGame.player_name(cell))
            output.append("\n")

        return "".join(output)

    def clone(self):
        new_state = OthelloState(self.board_size)

        for i in xrange(self.board_size):
            for j in xrange(self.board_size):
                new_state.board[i][j] = self.board[i][j]
        new_state.next_player = self.next_player

        return new_state

    def game_over(self):
        P1 = OthelloGame.PLAYER1
        P2 = OthelloGame.PLAYER2
        if self.player_moves[P1] is None:
            self.player_moves[P1] = self.generate_moves(P1)
        if self.player_moves[P2] is None:
            self.player_moves[P2] = self.generate_moves(P2)

        return not self.player_moves[P1] and not self.player_moves[P2]

    def score(self):
        score = 0

        P1 = OthelloGame.PLAYER1
        P2 = OthelloGame.PLAYER2
        board_size = self.board_size
        for i in xrange(board_size):
            for j in xrange(board_size):
                cell = self.board[i][j]
                if cell == P1:
                    score += 1
                if cell == P2:
                    score -= 1

        return score

    def __is_in_range(self, index):
        return index >= 0 and index < self.board_size

    def generate_moves(self, player=None):

        if player is None:
            player = self.next_player

        if self.player_moves[player]:
            return self.player_moves[player]

        other_player = OthelloGame.other_player(player)

        moves = list()
        moves_append = moves.append
        board_size = self.board_size

        # these two arrays encode the 8 posible
        # directions in which a player
        # can capture pieces:
        offs_x = [0, 1, 1, 1, 0, -1, -1, -1]
        offs_y = [-1, -1, 0, 1, 1, 1, 0, -1]

        NOTHING = OthelloGame.NOTHING

        for i in xrange(self.board_size):
            for j in xrange(self.board_size):
                if self.board[i][j] != NOTHING:
                    continue

                move_found = False

                for k in xrange(len(offs_x)):
                    if move_found:
                            break

                    current_x = i + offs_x[k]
                    current_y = j + offs_y[k]

                    while (((current_x + offs_x[k]) >= 0 and
                            (current_x + offs_x[k] < board_size) and
                            (current_y + offs_y[k]) >= 0) and
                            (current_y + offs_y[k] < board_size) and
                           (self.board[current_x][current_y] ==
                            other_player)):

                        current_x += offs_x[k]
                        current_y += offs_y[k]

                        if (self.board[current_x][current_y] == player):
                            # Legal move:
                            move_found = True
                            moves_append(OthelloMove(player, i, j))
                            break

        self.player_moves[player] = moves
        return moves

    def apply_move(self, move):
        self.player_moves[OthelloGame.PLAYER1] = None
        self.player_moves[OthelloGame.PLAYER2] = None
        self.next_player = OthelloGame.other_player(self.next_player)

        if not move:  # player passes
            return

        # set the piece:
        self.board[move.x][move.y] = move.player

        other_player = OthelloGame.other_player(move.player)
        move_x = move.x
        move_y = move.y
        move_player = move.player
        board_size = self.board_size

        # these two arrays encode the 8 posible directions in
        # which a player can capture pieces:
        offs_x = [0, 1, 1, 1, 0, -1, -1, -1]
        offs_y = [-1, -1, 0, 1, 1, 1, 0, -1]

        # see if any pieces are captured:
        for i in xrange(len(offs_x)):
            current_x = move_x + offs_x[i]
            current_y = move_y + offs_y[i]
            while ((current_x + offs_x[i] >= 0) and
                   (current_x + offs_x[i] < board_size) and
                   (current_y + offs_y[i] >= 0) and
                   (current_y + offs_y[i] < board_size) and
                   (self.board[current_x][current_y] ==
                    other_player)):

                current_x += offs_x[i]
                current_y += offs_y[i]

                if self.board[current_x][current_y] == move_player:
                    # pieces captured!:
                    reversed_x = move_x + offs_x[i]
                    reversed_y = move_y + offs_y[i]
                    while reversed_x != current_x or reversed_y != current_y:
                        self.board[reversed_x][reversed_y] = move_player
                        reversed_x += offs_x[i]
                        reversed_y += offs_y[i]
                    break

    def apply_move_cloning(self, move):
        """
            Creates a new games state that has the
            result of applying the move 'move'
        """

        new_state = self.clone()
        new_state.apply_move(move)
        return new_state


