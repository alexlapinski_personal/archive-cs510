Minimax vs Montecarlo Tree Search

Adding Minimax Player (max_depth: 3)
Adding Montecarlo Player (iterations: 100)

Current state, O to move
........
........
........
...OX...
...XO...
........
........
........

Player O to 2, 4

Current state, X to move
........
........
........
...OX...
..OOO...
........
........
........

Player X to 2, 3

Current state, O to move
........
........
........
..XXX...
..OOO...
........
........
........

Player O to 2, 2

Current state, X to move
........
........
..O.....
..OOX...
..OOO...
........
........
........

Player X to 4, 5

Current state, O to move
........
........
..O.....
..OOX...
..OOX...
....X...
........
........

Player O to 5, 2

Current state, X to move
........
........
..O..O..
..OOO...
..OOX...
....X...
........
........

Player X to 1, 2

Current state, O to move
........
........
.XO..O..
..XOO...
..OXX...
....X...
........
........

Player O to 0, 2

Current state, X to move
........
........
OOO..O..
..XOO...
..OXX...
....X...
........
........

Player X to 3, 2

Current state, O to move
........
........
OOOX.O..
..XXO...
..OXX...
....X...
........
........

Player O to 5, 6

Current state, X to move
........
........
OOOX.O..
..OXO...
..OOX...
....O...
.....O..
........

Player X to 5, 3

Current state, O to move
........
........
OOOX.O..
..OXXX..
..OOX...
....O...
.....O..
........

Player O to 4, 2

Current state, X to move
........
........
OOOOOO..
..OOOX..
..OOO...
....O...
.....O..
........

Player X to 3, 1

Current state, O to move
........
...X....
OOOOXO..
..OOOX..
..OOO...
....O...
.....O..
........

Player O to 3, 0

Current state, X to move
...O....
...O....
OOOOXO..
..OOOX..
..OOO...
....O...
.....O..
........

Player X to 1, 5

Current state, O to move
...O....
...O....
OOOOXO..
..OXOX..
..XOO...
.X..O...
.....O..
........

Player O to 6, 4

Current state, X to move
...O....
...O....
OOOOOO..
..OXOO..
..XOO.O.
.X..O...
.....O..
........

Player X to 6, 3

Current state, O to move
...O....
...O....
OOOOOO..
..OXXXX.
..XOO.O.
.X..O...
.....O..
........

Player O to 0, 6

Current state, X to move
...O....
...O....
OOOOOO..
..OOXXX.
..OOO.O.
.O..O...
O....O..
........

Player X to 2, 0

Current state, O to move
..XO....
...X....
OOOOXO..
..OOXXX.
..OOO.O.
.O..O...
O....O..
........

Player O to 7, 3

Current state, X to move
..XO....
...X....
OOOOXO..
..OOOOOO
..OOO.O.
.O..O...
O....O..
........

Player X to 4, 0

Current state, O to move
..XXX...
...X....
OOOOXO..
..OOOOOO
..OOO.O.
.O..O...
O....O..
........

Player O to 5, 1

Current state, X to move
..XXX...
...X.O..
OOOOOO..
..OOOOOO
..OOO.O.
.O..O...
O....O..
........

Player X to 3, 5

Current state, O to move
..XXX...
...X.O..
OOOXOO..
..OXOOOO
..OXO.O.
.O.XO...
O....O..
........

Player O to 2, 5

Current state, X to move
..XXX...
...X.O..
OOOXOO..
..OXOOOO
..OOO.O.
.OOOO...
O....O..
........

Player X to 3, 6

Current state, O to move
..XXX...
...X.O..
OOOXOO..
..OXOOOO
..OXO.O.
.OOXO...
O..X.O..
........

Player O to 4, 1

Current state, X to move
..XXX...
...XOO..
OOOOOO..
..OXOOOO
..OXO.O.
.OOXO...
O..X.O..
........

Player X to 5, 4

Current state, O to move
..XXX...
...XOO..
OOOOOO..
..OXOOOO
..OXXXO.
.OOXX...
O..X.O..
........

Player O to 3, 7

Current state, X to move
..XXX...
...XOO..
OOOOOO..
..OOOOOO
..OOXXO.
.OOOX...
O..O.O..
...O....

Player X to 5, 0

Current state, O to move
..XXXX..
...XOX..
OOOOOX..
..OOOXOO
..OOXXO.
.OOOX...
O..O.O..
...O....

Player O to 6, 0

Current state, X to move
..XXXXO.
...XOO..
OOOOOX..
..OOOXOO
..OOXXO.
.OOOX...
O..O.O..
...O....

Player X to 7, 0

Current state, O to move
..XXXXXX
...XOO..
OOOOOX..
..OOOXOO
..OOXXO.
.OOOX...
O..O.O..
...O....

Player O to 4, 6

Current state, X to move
..XXXXXX
...XOO..
OOOOOX..
..OOOXOO
..OOOXO.
.OOOO...
O..OOO..
...O....

Player X to 7, 2

Current state, O to move
..XXXXXX
...XOO..
OOOOOX.X
..OOOXXO
..OOOXO.
.OOOO...
O..OOO..
...O....

Player O to 6, 5

Current state, X to move
..XXXXXX
...XOO..
OOOOOX.X
..OOOXXO
..OOOOO.
.OOOO.O.
O..OOO..
...O....

Player X to 1, 4

Current state, O to move
..XXXXXX
...XXO..
OOOXOX.X
..XOOXXO
.XOOOOO.
.OOOO.O.
O..OOO..
...O....

Player O to 1, 3

Current state, X to move
..XXXXXX
...XXO..
OOOXOX.X
.OOOOXXO
.OOOOOO.
.OOOO.O.
O..OOO..
...O....

Player X to 5, 5

Current state, O to move
..XXXXXX
...XXO..
OOOXOX.X
.OOOOXXO
.OOOOXO.
.OOOOXO.
O..OOO..
...O....

Player O to 6, 2

Current state, X to move
..XXXXXX
...XXO..
OOOXOOOX
.OOOOOOO
.OOOOXO.
.OOOOXO.
O..OOO..
...O....

Player X to 2, 7

Current state, O to move
..XXXXXX
...XXO..
OOOXOOOX
.OOOOOOO
.OOOOXO.
.OOOXXO.
O..XOO..
..XO....

Player O to 2, 6

Current state, X to move
..XXXXXX
...XXO..
OOOXOOOX
.OOOOOOO
.OOOOXO.
.OOOXXO.
O.OOOO..
..XO....

Player X to 6, 1

Current state, O to move
..XXXXXX
...XXXX.
OOOXOOOX
.OOOOOOO
.OOOOXO.
.OOOXXO.
O.OOOO..
..XO....

Player O to 2, 1

Current state, X to move
..XXXXXX
..OXXXX.
OOOOOOOX
.OOOOOOO
.OOOOXO.
.OOOXXO.
O.OOOO..
..XO....

Player X to 5, 7

Current state, O to move
..XXXXXX
..OXXXX.
OOOOOOOX
.OOOOOOO
.OOOOXO.
.OOOXXO.
O.OOOX..
..XO.X..

Player O to 7, 1

Current state, X to move
..XXXXXX
..OOOOOO
OOOOOOOO
.OOOOOOO
.OOOOXO.
.OOOXXO.
O.OOOX..
..XO.X..

Player X to 7, 6

Current state, O to move
..XXXXXX
..OOOOOO
OOOOOOOO
.OOOOOOO
.OOOOXO.
.OOOXXX.
O.OOOX.X
..XO.X..

Player O to 1, 7

Current state, X to move
..XXXXXX
..OOOOOO
OOOOOOOO
.OOOOOOO
.OOOOXO.
.OOOXXX.
O.OOOX.X
.OOO.X..

Player X to 7, 4

Current state, O to move
..XXXXXX
..OOXOOX
OOOOOXOX
.OOOOOXX
.OOOOXXX
.OOOXXX.
O.OOOX.X
.OOO.X..

Player O to 7, 5

Current state, X to move
..XXXXXX
..OOXOOX
OOOOOXOX
.OOOOOXX
.OOOOXOX
.OOOOOOO
O.OOOX.X
.OOO.X..

Player X to 4, 7

Current state, O to move
..XXXXXX
..OOXOOX
OOOOXXOX
.OOOXOXX
.OOOXXOX
.OOOXOOO
O.OOXX.X
.OOOXX..

Player O to 6, 7

Current state, X to move
..XXXXXX
..OOXOOX
OOOOXXOX
.OOOXOXX
.OOOXXOX
.OOOOOOO
O.OOXO.X
.OOOOOO.

Player X to 1, 0

Current state, O to move
.XXXXXXX
..XOXOOX
OOOXXXOX
.OOOXOXX
.OOOXXOX
.OOOOOOO
O.OOXO.X
.OOOOOO.

Player O to 1, 1

Current state, X to move
.XXXXXXX
.OOOXOOX
OOOXXXOX
.OOOXOXX
.OOOXXOX
.OOOOOOO
O.OOXO.X
.OOOOOO.

Player X to 0, 3

Current state, O to move
.XXXXXXX
.OXOXOOX
OXOXXXOX
XXXXXOXX
.OOOXXOX
.OOOOOOO
O.OOXO.X
.OOOOOO.

Player O to 0, 4

Current state, X to move
.XXXXXXX
.OXOXOOX
OXOXXXOX
OOXXXOXX
OOOOXXOX
.OOOOOOO
O.OOXO.X
.OOOOOO.

Player X to 1, 6

Current state, O to move
.XXXXXXX
.OXOXOOX
OXOXXXOX
OXXXXOXX
OXOXXXOX
.XXOOOOO
OXXXXO.X
.OOOOOO.

Player O to 0, 7

Current state, X to move
.XXXXXXX
.OXOXOOX
OXOXXOOX
OXXXOOXX
OXOOXXOX
.XOOOOOO
OOXXXO.X
OOOOOOO.

Player X to 6, 6

Current state, O to move
.XXXXXXX
.OXOXOOX
OXOXXOOX
OXXXOOXX
OXOOXXXX
.XOOOXXO
OOXXXXXX
OOOOOOO.

Player O to 0, 1

Current state, X to move
.XXXXXXX
OOXOXOOX
OOOXXOOX
OXOXOOXX
OXOOXXXX
.XOOOXXO
OOXXXXXX
OOOOOOO.

Player X to 0, 0

Current state, O to move
XXXXXXXX
OXXOXOOX
OOXXXOOX
OXOXOOXX
OXOOXXXX
.XOOOXXO
OOXXXXXX
OOOOOOO.

Player O to 0, 5

Current state, X to move
XXXXXXXX
OXXOXOOX
OOXXXOOX
OXOXOOXX
OOOOXXXX
OOOOOXXO
OOXXXXXX
OOOOOOO.

None

Current state, O to move
XXXXXXXX
OXXOXOOX
OOXXXOOX
OXOXOOXX
OOOOXXXX
OOOOOXXO
OOXXXXXX
OOOOOOO.

Player O to 7, 7

Final state with score: 2
XXXXXXXX
OXXOXOOX
OOXXXOOX
OXOXOOXX
OOOOXXXX
OOOOOXXO
OOXXXXXO
OOOOOOOO

Time Elapsed 8.913557sec

Montecarlo Tree Search vs Minimax

Adding Montecarlo Player (iterations: 100)
Adding Minimax Player (max_depth: 3)

Current state, O to move
........
........
........
...OX...
...XO...
........
........
........

Player O to 3, 5

Current state, X to move
........
........
........
...OX...
...OO...
...O....
........
........

Player X to 2, 3

Current state, O to move
........
........
........
..XXX...
...OO...
...O....
........
........

Player O to 2, 2

Current state, X to move
........
........
..O.....
..XOX...
...OO...
...O....
........
........

Player X to 2, 1

Current state, O to move
........
..X.....
..X.....
..XOX...
...OO...
...O....
........
........

Player O to 1, 3

Current state, X to move
........
..X.....
..X.....
.OOOX...
...OO...
...O....
........
........

Player X to 0, 3

Current state, O to move
........
..X.....
..X.....
XXXXX...
...OO...
...O....
........
........

Player O to 1, 1

Current state, X to move
........
.OX.....
..O.....
XXXOX...
...OO...
...O....
........
........

Player X to 0, 1

Current state, O to move
........
XXX.....
..O.....
XXXOX...
...OO...
...O....
........
........

Player O to 0, 0

Current state, X to move
O.......
XOX.....
..O.....
XXXOX...
...OO...
...O....
........
........

Player X to 4, 5

Current state, O to move
O.......
XOX.....
..O.....
XXXOX...
...XX...
...OX...
........
........

Player O to 2, 0

Current state, X to move
O.O.....
XOO.....
..O.....
XXXOX...
...XX...
...OX...
........
........

Player X to 2, 5

Current state, O to move
O.O.....
XOO.....
..O.....
XXXOX...
...XX...
..XXX...
........
........

Player O to 0, 2

Current state, X to move
O.O.....
OOO.....
O.O.....
XXXOX...
...XX...
..XXX...
........
........

Player X to 3, 1

Current state, O to move
O.O.....
OOOX....
O.X.....
XXXOX...
...XX...
..XXX...
........
........

Player O to 0, 4

Current state, X to move
O.O.....
OOOX....
O.X.....
OXXOX...
O..XX...
..XXX...
........
........

Player X to 3, 2

Current state, O to move
O.O.....
OOOX....
O.XX....
OXXXX...
O..XX...
..XXX...
........
........

Player O to 4, 2

Current state, X to move
O.O.....
OOOO....
O.XXO...
OXXXX...
O..XX...
..XXX...
........
........

Player X to 4, 0

Current state, O to move
O.O.X...
OOOX....
O.XXO...
OXXXX...
O..XX...
..XXX...
........
........

Player O to 4, 1

Current state, X to move
O.O.X...
OOOOO...
O.XXO...
OXXXX...
O..XX...
..XXX...
........
........

Player X to 1, 0

Current state, O to move
OXO.X...
OOXOO...
O.XXO...
OXXXX...
O..XX...
..XXX...
........
........

Player O to 1, 2

Current state, X to move
OXO.X...
OOXOO...
OOOOO...
OXXXX...
O..XX...
..XXX...
........
........

Player X to 5, 0

Current state, O to move
OXO.XX..
OOXOX...
OOOXO...
OXXXX...
O..XX...
..XXX...
........
........

Player O to 5, 6

Current state, X to move
OXO.XX..
OOXOX...
OOOXO...
OXOXX...
O..OX...
..XXO...
.....O..
........

Player X to 5, 1

Current state, O to move
OXO.XX..
OOXOXX..
OOOXX...
OXOXX...
O..OX...
..XXO...
.....O..
........

Player O to 1, 6

Current state, X to move
OXO.XX..
OOXOXX..
OOOXX...
OXOXX...
O..OX...
..OXO...
.O...O..
........

Player X to 0, 7

Current state, O to move
OXO.XX..
OOXOXX..
OOOXX...
OXOXX...
O..XX...
..XXO...
.X...O..
X.......

Player O to 5, 3

Current state, X to move
OXO.XX..
OOXOXX..
OOOXO...
OXOOOO..
O..XX...
..XXO...
.X...O..
X.......

Player X to 6, 2

Current state, O to move
OXO.XX..
OOXOXX..
OOOXO.X.
OXOOOX..
O..XX...
..XXO...
.X...O..
X.......

Player O to 1, 4

Current state, X to move
OXO.XX..
OOXOXX..
OOOXO.X.
OOOOOX..
OO.XX...
..XXO...
.X...O..
X.......

Player X to 6, 7

Current state, O to move
OXO.XX..
OOXOXX..
OOOXO.X.
OOOOOX..
OO.XX...
..XXX...
.X...X..
X.....X.

Player O to 6, 3

Current state, X to move
OXO.XX..
OOXOXX..
OOOXO.X.
OOOOOOO.
OO.XX...
..XXX...
.X...X..
X.....X.

Player X to 1, 5

Current state, O to move
OXO.XX..
OXXOXX..
OXOXO.X.
OXOOOOO.
OX.XX...
.XXXX...
.X...X..
X.....X.

Player O to 3, 0

Current state, X to move
OXOOXX..
OXOOXX..
OOOXO.X.
OXOOOOO.
OX.XX...
.XXXX...
.X...X..
X.....X.

Player X to 7, 3

Current state, O to move
OXOOXX..
OXOOXX..
OOOXO.X.
OXXXXXXX
OX.XX...
.XXXX...
.X...X..
X.....X.

Player O to 5, 4

Current state, X to move
OXOOXX..
OXOOXX..
OOOOO.X.
OXXXOXXX
OX.XXO..
.XXXX...
.X...X..
X.....X.

Player X to 6, 5

Current state, O to move
OXOOXX..
OXXOXX..
OOOXO.X.
OXXXXXXX
OX.XXX..
.XXXX.X.
.X...X..
X.....X.

Player O to 1, 7

Current state, X to move
OXOOXX..
OXXOXX..
OOOXO.X.
OOXXXXXX
OO.XXX..
.OXXX.X.
.O...X..
XO....X.

Player X to 2, 7

Current state, O to move
OXOOXX..
OXXOXX..
OOOXO.X.
OOXXXXXX
OO.XXX..
.OXXX.X.
.O...X..
XXX...X.

Player O to 2, 4

Current state, X to move
OXOOXX..
OXXOXX..
OOOXO.X.
OOOOXXXX
OOOXXX..
.OXXX.X.
.O...X..
XXX...X.

Player X to 0, 5

Current state, O to move
OXOOXX..
OXXOXX..
OOOXO.X.
OOXOXXXX
OXOXXX..
XXXXX.X.
.X...X..
XXX...X.

Player O to 5, 2

Current state, X to move
OXOOXX..
OXXOOX..
OOOXOOX.
OOXOXXXX
OXOXXX..
XXXXX.X.
.X...X..
XXX...X.

None

Current state, O to move
OXOOXX..
OXXOOX..
OOOXOOX.
OOXOXXXX
OXOXXX..
XXXXX.X.
.X...X..
XXX...X.

Player O to 3, 6

Current state, X to move
OXOOXX..
OXXOOX..
OOOXOOX.
OOXOXXXX
OOOOXX..
XXOOX.X.
.X.O.X..
XXX...X.

Player X to 3, 7

Current state, O to move
OXOOXX..
OXXOOX..
OOOXOOX.
OOXXXXXX
OOOXXX..
XXOXX.X.
.X.X.X..
XXXX..X.

Player O to 7, 4

Current state, X to move
OXOOXX..
OXXOOX..
OOOXOOX.
OOXXXXOX
OOOXXX.O
XXOXX.X.
.X.X.X..
XXXX..X.

Player X to 6, 1

Current state, O to move
OXOOXX..
OXXOOXX.
OOOXOXX.
OOXXXXOX
OOOXXX.O
XXOXX.X.
.X.X.X..
XXXX..X.

Player O to 7, 1

Current state, X to move
OXOOXX..
OXXOOOOO
OOOXOXX.
OOXXXXOX
OOOXXX.O
XXOXX.X.
.X.X.X..
XXXX..X.

Player X to 7, 2

Current state, O to move
OXOOXX..
OXXOOOXO
OOOXOXXX
OOXXXXXX
OOOXXX.O
XXOXX.X.
.X.X.X..
XXXX..X.

Player O to 5, 5

Current state, X to move
OXOOXX..
OXXOOOXO
OOOXOOXX
OOXOXOXX
OOOXOO.O
XXOOOOX.
.X.X.X..
XXXX..X.

Player X to 2, 6

Current state, O to move
OXOOXX..
OXXOOOXO
OOOXOOXX
OOXOXXXX
OOXXXO.O
XXXXOOX.
.XXX.X..
XXXX..X.

Player O to 4, 6

Current state, X to move
OXOOXX..
OXXOOOXO
OOOXOOXX
OOXOXXXX
OOOXXO.O
XXXOOOX.
.XXXOX..
XXXX..X.

Player X to 4, 7

Current state, O to move
OXOOXX..
OXXOOOXO
OOOXOOXX
OOXOXXXX
OOOXXO.O
XXXOXOX.
.XXXXX..
XXXXX.X.

Player O to 0, 6

Current state, X to move
OXOOXX..
OXXOOOXO
OOOXOOXX
OOXOXXXX
OOOXXO.O
OOXOXOX.
OXXXXX..
XXXXX.X.

Player X to 7, 0

Current state, O to move
OXOOXX.X
OXXOOOXX
OOOXOOXX
OOXOXXXX
OOOXXO.O
OOXOXOX.
OXXXXX..
XXXXX.X.

Player O to 6, 6

Current state, X to move
OXOOXX.X
OXXOOOXX
OOOXOOXX
OOXOXXXX
OOOXXO.O
OOXOXOX.
OOOOOOO.
XXXXX.X.

Player X to 7, 5

Current state, O to move
OXOOXX.X
OXXOOOXX
OOOXOOXX
OOXOXXXX
OOOXXO.X
OOXOXOXX
OOOOOOO.
XXXXX.X.

Player O to 6, 0

Current state, X to move
OXOOOOOX
OXXOOOXX
OOOXOOXX
OOXOXXXX
OOOXXO.X
OOXOXOXX
OOOOOOO.
XXXXX.X.

Player X to 6, 4

Current state, O to move
OXOOOOOX
OXXOOOXX
OOOXOOXX
OOXOXXXX
OOOXXXXX
OOXOXXXX
OOOOXOO.
XXXXX.X.

Player O to 5, 7

Current state, X to move
OXOOOOOX
OXXOOOXX
OOOXOOXX
OOXOXXXX
OOOXXXXX
OOXOXXXX
OOOOOOO.
XXXXXOX.

Player X to 7, 7

Current state, O to move
OXOOOOOX
OXXOOOXX
OOOXOOXX
OOXOXXXX
OOOXXXXX
OOXOXXXX
OOOOOOX.
XXXXXOXX

Player O to 7, 6

Final state with score: 2
OXOOOOOX
OXXOOOXX
OOOXOOXX
OOXOXXXX
OOOXXXXX
OOXOXXXX
OOOOOOOO
XXXXXOXX

Time Elapsed 5.7497sec
