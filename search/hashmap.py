
class HashMap():
    """
        A simple has map to contain a set of nodes.
        Provides constant lookup time.
        Keys are the normalized node state expressed as a string
        the node's state takes all rows,
        concatenates rows into one list
    """

    def __init__(self):
        self._dictionary = {}
        self._keys = []

    def __len__(self):
        return len(self._keys)

    def push(self, node):
        """
            Add a node to the map if it does not exist
        """

        key = self._generate_key(node.state)

        if key not in self._dictionary:
            self._dictionary[key] = node
            self._keys.append(key)

    def pop(self):
        """
        Return the first item added
        """
        if not self._keys:
            return None

        key = self._keys.pop()
        value = self._dictionary[key]
        del self._dictionary[key]
        return value

    def removeState(self, state):
        key = self._generate_key(state)
        # Remove from the keys
        self._keys.remove(key)

        # Remove from the map
        del self._dictionary[key]

    def contains_state(self, state):
        key = self._generate_key(state)
        return key in self._dictionary

    def _generate_key(self, state):
        if state is None:
            return None

        row_keys = []
        normalizedState = state.normalizeState()
        for row in normalizedState.rows:
            row_keys.append(''.join([str(cell) for cell in row]))

        return ''.join(row_keys)
