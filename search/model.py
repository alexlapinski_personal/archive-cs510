from mutator import applyMoveCloning


class Node:
    """
        A representation of a node in a solution graph.
        The basis of this class comes from page 79 of Artifical Intelligence,
        A Modern Approach. Third Edition
    """

    def __init__(self, state, parent=None, move=None):
        self.state = state
        self.parent = parent
        self.move = move
        self.g = 0
        self.h = 0

    def __eq__(self, other):
        if other is None:
            return False

        if self.state != other.state:
            return False

        if self.parent != other.parent:
            return False

        if self.move != other.Parent:
            return False

        return True

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return "Node(g={0}, h={1}, move={2}, hasParent={3}, state={4}".format(
                     self.g, self.h, self.move,
                     self.parent is not None, self.state)

    def childNode(self, move):
        """
            Return a child of this node given a move to apply.
            This correctly links the generated child to this node as it's
            parent.
        """
        childState = applyMoveCloning(self.state, move)
        return Node(childState, self, move)


class Solution:
    """
        A simple container for the solution found by DFS or BFS as well
        as stats
    """

    def __init__(self, nodesExplored, finalNode):
        self.nodesExplored = nodesExplored

        if finalNode is not None:
            self.moves = self._solution(finalNode)
            self.solutionLength = len(self.moves)
            self.finalState = finalNode.state
        else:
            self.moves = list()
            self.solutionLength = -1
            self.finalState = None

    def _solution(self, goalNode):
        """
            Extract a list of moves working back up from the goal node to the
            initialState
        """
        moves = list()
        node = goalNode
        while node:
            if node.move:
                moves.append(node.move)
            node = node.parent
        # Moves are added in reverse order, so reverse the result
        moves.reverse()
        return moves
