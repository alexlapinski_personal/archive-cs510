import heapq
from .hashmap import HashMap


class PriorityQueue:
    """A simple priority queue of nodes in aStar"""

    def __init__(self):
        self._queue = []
        self._index = 0
        self._hashmap = HashMap()

    def push(self, node):
        # Priority is the g(n) + h(h) = f(n)
        priority = (node.g + node.h)
        # Push a tuple onto the queue
        heapq.heappush(self._queue, (priority, self._index, node))
        self._hashmap.push(node)

        # Our index makes sure we pick the first added entry
        # when multiple entries have the same priority
        self._index += 1

    def pop(self):
        # Get the actual node from the tuple inserted
        node = heapq.heappop(self._queue)[-1]
        self._hashmap.removeState(node.state)
        return node

    def __len__(self):
        return len(self._queue)

    def __contains__(self, value):
        valueNormalized = value.state.normalizeState()
        return self._hashmap.contains_state(valueNormalized)

    def __nonzero__(self):
        """
            Convience function to help evaluate this object
            in boolean expressions
        """
        return len(self._queue)
