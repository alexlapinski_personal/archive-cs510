from search.model import Node, Solution
from search.queue import PriorityQueue
from solver import gameStateSolved, allMoves
from search.hashmap import HashMap


def search(initialState, heuristic, pathCost=1):

    start = Node(initialState)
    start.g = 0
    start.h = heuristic(start.state)
    frontier = PriorityQueue()
    frontier.push(start)
    explored = HashMap()

    # While OPEN is not empty
    while frontier:

        # N = OPEN.removeLowestF()
        node = frontier.pop()

        # IF Goal(N) RETURN path to N
        if gameStateSolved(node.state):
            return Solution(len(explored), node)

        # CLOSED.add(N)
        explored.push(node)

        # FOR all children M of N not in CLOSED
        moves = allMoves(node.state)
        for child in [node.childNode(move) for move in moves]:
            # N not in CLOSED
            childStateNormalized = child.state.normalizeState()
            if not explored.contains_state(childStateNormalized):
                # update node
                child.g = node.g + pathCost  # Assume uniform path cost
                child.h = heuristic(child.state)

                # OPEN.add(M)
                if child not in frontier:
                    frontier.push(child)
