from collections import deque
from search.model import Node, Solution
from searchUtils import isNodeInList
from solver import gameStateSolved, allMoves


def search(initialState):
    """
        An implementation of breadth first search to search for a solution
        path to the goal state of the Sliding Board Puzzle.
        Returns a Solution object.
        The basis of this algorithm is adapted from the 'Breadth First Search'
        on page 82 of Artifical Intelligence, A Modern Approach. Third Edition.
    """
    # Get a queue to hold our space of nodes to visit & add the initial state
    frontier = deque()
    frontier.append(Node(initialState))

    # A list to hold what we've explored
    explored = list()

    # The node to return, or None if a goal node is not found
    while frontier:
        node = frontier.popleft()  # Get the node from left of the queue
        explored.append(node)

        # Find moves that generate nodes
        moves = allMoves(node.state)
        for move in moves:
            child = node.childNode(move)
            # Only look at the child generated node if we haven't seen it,
            # and it isn't up for exploration (prevent loops)
            isNodeInFrontier = isNodeInList(child, frontier)
            hasNodeBeenExplored = isNodeInList(child, explored)

            if not isNodeInFrontier and not hasNodeBeenExplored:
                # Check to see if it is a solution, if so, return it
                if gameStateSolved(child.state):
                    return Solution(len(explored), child)
                else:
                    frontier.append(child)

    return Solution(len(explored), None)
