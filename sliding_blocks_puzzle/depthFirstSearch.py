from solver import allMoves, gameStateSolved
from search.model import Node, Solution
from searchUtils import isNodeInList


def search(initialState):
    """
        Implementation of Depth First Search to solve the
        Sliding Block Puzzle game.
        Return a Solution object.
        The basis of this algorithm is adapted from the 'GraphSearch' on
        page 77 of Artifical Intelligence, A Modern Approach. Third Edition.
    """

    # Lists to hold our explored / nodes to search list is LIFO
    # (last in first out)
    explored = list()
    frontier = list()
    frontier.append(Node(initialState))  # Add our initial state

    goalNode = None
    while len(frontier) > 0:  # exit if we have no search space
        node = frontier.pop()
        if gameStateSolved(node.state):
            goalNode = node  # found the solution, exit
            break

        # once we've taken most recently added node, mark it explored
        explored.append(node)

        moves = allMoves(node.state)
        for move in moves:
            # Iterate of possible nodes not yet explored or
            # not in our search space
            child = node.childNode(move)
            isNodeInFrontier = isNodeInList(child, frontier)
            hasNodeBeenExplored = isNodeInList(child, explored)
            if not isNodeInFrontier and not hasNodeBeenExplored:
                frontier.append(child)

    return Solution(len(explored), goalNode)
