from state import Board, find_piece


def manhattan_distance(currentState):
    """
        A heuristic to be used with A Star algorithm.
        Computes the manhattan distance between the master brick and
        the goal brick.

        The manhattan distance is the number of tiles between two tiles in
        two dimensions. We count the horizontal and vertical tiles and
        add the number between.
    """

    if currentState is None:
        return 0

    if currentState.width == 0 and currentState.height == 0:
        return 0

    # Determine the place of the Master Brick
    masterBrick = find_piece(Board.MasterBrickTile, currentState)

    # Determine the place of the Goal Brick
    goalTile = find_piece(Board.GoalTile, currentState)

    # If there is no master brick, return 0
    if masterBrick is None:
        return 0

    # If there is no goal tile, return 0
    if goalTile is None:
        return 0

    masterBrickRightColIndex = masterBrick.colIndex + masterBrick.width - 1

    if masterBrick.colIndex > goalTile.colIndex:
        horizontal_distance = abs(goalTile.colIndex - masterBrick.colIndex)
    elif masterBrick.colIndex < goalTile.colIndex:
        horizontal_distance = abs(goalTile.colIndex - masterBrickRightColIndex)
    else:
        horizontal_distance = 0

    masterBrickBottomRowIndex = masterBrick.rowIndex + masterBrick.height - 1

    if masterBrick.rowIndex > goalTile.rowIndex:
        vertical_distance = abs(goalTile.rowIndex - masterBrick.rowIndex)
    elif masterBrick.rowIndex < goalTile.rowIndex:
        vertical_distance = abs(goalTile.rowIndex - masterBrickBottomRowIndex)
    else:
        vertical_distance = 0

    return vertical_distance + horizontal_distance


def manhattan_distance_cust(currentState):
    """
        A heuristic to be used with A Star algorithm.
        Computes the manhattan distance between the master brick and
        the goal brick.

        The manhattan distance is the number of tiles between two tiles in
        two dimensions. We count the horizontal and vertical tiles and
        add the number between.
    """

    manhattanDistance = manhattan_distance(currentState)

    masterBrick = find_piece(Board.MasterBrickTile, currentState)
    goalTile = find_piece(Board.GoalTile, currentState)

    # We're going to check to see if there are any tiles
    # between the masterBrick's top-left corner and the
    # goal's top-left corner

    blockers = 0

    if masterBrick.rowIndex == goalTile.rowIndex:
        # search that row
        for colIndex in range(0, currentState.width):
            cell = currentState.rows[goalTile.rowIndex][colIndex]
            if cell not in (-1, 0, 2, 1):
                blockers += 1

    if masterBrick.colIndex == goalTile.colIndex:
        # search that row
        for rowIndex in range(0, currentState.height):
            cell = currentState.rows[rowIndex][goalTile.colIndex]
            if cell not in (-1, 0, 2, 1):
                blockers += 1

    return manhattanDistance + blockers
