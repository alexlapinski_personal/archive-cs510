class Move:
    """Representation of a move in the Sliding Block Puzzle game"""

    Up = 0
    Down = 1
    Left = 2
    Right = 3

    def __init__(self, pieceNumber, direction):
        """
            Create a new move, where pieceNumber is the number of the piece,
             and direction is 'Up', 'Down', 'Left' or 'Right'
        """
        self.__pieceNumber = pieceNumber
        self.direction = direction

    @property
    def pieceNumber(self):
        return self.__pieceNumber

    @property
    def direction(self):
        return self.__direction

    @direction.setter
    def direction(self, value):
        self.__direction = value


    def __eq__(self, other):
        if other is None:
            return False

        if self.pieceNumber != other.pieceNumber:
            return False

        if self.direction != other.direction:
            return False

        return True

    def __str__(self):
        return 'move({0},{1})'.format(self.pieceNumber,
                                      directionToString(self.direction))


def directionToString(direction):
    """Simple translator of Direction enum int to string"""
    if direction == Move.Up:
        return 'Up'
    if direction == Move.Down:
        return 'Down'
    if direction == Move.Left:
        return 'Left'
    if direction == Move.Right:
        return 'Right'
    return 'Unknown'
