from state import Board, cloneGameState
from move import Move


def applyMoveCloning(board, move):
    """
        Given a board at a current state, and a move, apply the move
        to the targetBoard
    """
    return __applyMoveImplementation(board, cloneGameState(board), move)


def applyMove(board, move):
    """Given a board at a current state, apply a move directly to the board"""
    # don't return, we modified the input
    __applyMoveImplementation(board, board, move)


def __cleanDestCell(destCell, move):
    """
        Shared logic to replace the goal tile with empty tile if
        the master brick is moving into the destCell
    """
    result = destCell

    isDestinationGoalTile = destCell == Board.GoalTile
    isMovePiceMasterTole = move.pieceNumber == Board.MasterBrickTile

    if isDestinationGoalTile and isMovePiceMasterTole:
        result = Board.EmptyTile

    return result


def __applyMoveImplementation(board, targetBoard, move):
    """
        The real implementation of both applyMove and applyMoveCloning,
        just accept 2 boards and allow unique method signatures
    """

    # Apply Move Up
    # Iterate Rows from top-down swapping tiles
    if move.direction == Move.Up:
        for rowIndex in range(board.height):
            for colIndex in range(board.width):
                cell = board.rows[rowIndex][colIndex]
                if cell == move.pieceNumber:
                    # Swap the cell with the one above it
                    destCell = targetBoard.rows[rowIndex-1][colIndex]
                    destCell = __cleanDestCell(destCell, move)
                    targetBoard.rows[rowIndex][colIndex] = destCell
                    targetBoard.rows[rowIndex-1][colIndex] = cell

    # Apply Move Down
    # Similar to Move up, but reverse direction of iterating rows
    if move.direction == Move.Down:
        for rowIndex in range(board.height-1, 0, -1):
            for colIndex in range(board.width):
                cell = board.rows[rowIndex][colIndex]
                if cell == move.pieceNumber:
                    # Swap the cell with target one below it
                    destCell = targetBoard.rows[rowIndex+1][colIndex]
                    destCell = __cleanDestCell(destCell, move)
                    targetBoard.rows[rowIndex][colIndex] = destCell
                    targetBoard.rows[rowIndex+1][colIndex] = cell

    # Apply Move Left
    # Similar to Move Up, but we move swap horizontally
    if move.direction == Move.Left:
        for rowIndex in range(board.height):
            for colIndex in range(board.width):
                cell = board.rows[rowIndex][colIndex]
                if cell == move.pieceNumber:
                    destCell = targetBoard.rows[rowIndex][colIndex-1]
                    destCell = __cleanDestCell(destCell, move)
                    targetBoard.rows[rowIndex][colIndex-1] = move.pieceNumber
                    targetBoard.rows[rowIndex][colIndex] = destCell

    # Apply Move Right
    # Similar to Move Left, but we look from the rightmost column to leftmost
    if move.direction == Move.Right:
        for rowIndex in range(board.height):
            for colIndex in range(board.width-1, 0, -1):
                cell = board.rows[rowIndex][colIndex]
                if cell == move.pieceNumber:
                    destCell = targetBoard.rows[rowIndex][colIndex+1]
                    destCell = __cleanDestCell(destCell, move)
                    targetBoard.rows[rowIndex][colIndex+1] = move.pieceNumber
                    targetBoard.rows[rowIndex][colIndex] = destCell

    return targetBoard
