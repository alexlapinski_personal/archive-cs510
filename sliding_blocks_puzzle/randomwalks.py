import random
from state import outputGameState
from mutator import applyMove
from solver import allMoves, gameStateSolved


def randomWalks(board, n):
    """
        Generate a list of all possible moves given the current board state.
        Pick a move at random. Apply that move, if we have reached
        the goal state, or the number of moves applied equals 'N', stop.
        Otherwise, continue to generate moves, normalize the board state
        and apply random moves.
    """

    # print the initial state
    outputGameState(board)
    numberOfMovesApplied = 0

    # Exit if we've reached a goal or exhausted the # walks alotted
    while not gameStateSolved(board) and numberOfMovesApplied < n:
        possibleMoves = allMoves(board)
        randomMove = random.choice(possibleMoves)

        # Print the move (ala Move.__str__)
        print randomMove
        print

        # Apply it directly, there's no going back
        applyMove(board, randomMove)
        outputGameState(board)
        numberOfMovesApplied = numberOfMovesApplied + 1
