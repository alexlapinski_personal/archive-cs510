import sys
import time
from state import loadGameState
import breadthFirstSearch
import depthFirstSearch
import aStar
from randomwalks import randomWalks
import heuristics


class SBPRunner:

    @staticmethod
    def print_usage():
        print "usage: python main.py <algorithmName> <inputFilepath> [stats]"
        print "where <algorithmName> is:"
        print "      aStar:heuristicName - Where 'heursticName' is the name"
        print "                            of the heuristic to use"
        print "      breadthFirstSearch"
        print "      depthFirstSearch"
        print "      randomWalks:N - where 'N' is the number of walks to take"
        print
        print "[stats] is optional and if present as a third argument prints"
        print " information about the solution, time, length, etc."

    @classmethod
    def run(cls, arguments):
        if len(arguments) < 3:
            cls.print_usage()

        algorithm = arguments[1]
        filename = arguments[2]
        show_stats = len(arguments) > 3 and arguments[3] is not None

        initialState = loadGameState(filename)

        if algorithm.startswith('randomWalks'):
            parts = algorithm.split(':')
            N = parts[1]
            start = time.clock()
            randomWalks(initialState, int(N))
            end = time.clock()
            if show_stats:
                cls.__printTimeElapsed(start, end)

        if algorithm == 'breadthFirstSearch':
            start = time.clock()
            solution = breadthFirstSearch.search(initialState)
            end = time.clock()

            cls.__printSolution(solution)

            if show_stats:
                cls.__printStats(solution, start, end)

        if algorithm == 'depthFirstSearch':
            start = time.clock()
            solution = depthFirstSearch.search(initialState)
            end = time.clock()

            cls.__printSolution(solution)

            if show_stats:
                cls.__printStats(solution, start, end)

        if algorithm.startswith("aStar"):
            heuristicName = algorithm.split(":")[1]
            if heuristicName == "manhattanDistance":
                heuristic = heuristics.manhattan_distance
            elif heuristicName == "custom":
                heuristic = heuristics.manhattan_distance_cust
            else:
                print "Unknown aStar heuristic '" + heuristicName + "'"

            start = time.clock()
            solution = aStar.search(initialState, heuristic)
            end = time.clock()

            cls.__printSolution(solution)

            if show_stats:
                cls.__printStats(solution, start, end)

    @staticmethod
    def __printSolution(solution):
        if not solution or not solution.moves:
            print "No solution found"
        else:
            for move in solution.moves:
                print move
            print solution.finalState

    @classmethod
    def __printStats(cls, solution, start, end):
        print "Nodes Explored: {0}".format(solution.nodesExplored)
        print "Solution Length: {0}".format(solution.solutionLength)
        cls.__printTimeElapsed(start, end)

    @staticmethod
    def __printTimeElapsed(start, end):
        """Print the difference between two calls 'time.clock()'"""
        millisecondsPerSecond = 1000
        # diff is in seconds (represented as floating point)
        diff = end - start
        if diff < 1.0:
            print 'Time Elapsed {0}ms'.format(diff * millisecondsPerSecond)
        else:
            print 'Time Elapsed {0}sec'.format(diff)


if __name__ == "__main__":
    SBPRunner.run(sys.argv)
