from state import stateEqual


def isNodeInList(node, list):
    """
        Check to see if a node is in the given list by comparing the states,
        returns True if it is in the list.
    """
    inList = False
    for candidate in list:
        stateInList = candidate.state.normalizeState()
        state = node.state.normalizeState()
        if stateEqual(state, stateInList):
            inList = True
            break
    return inList


def solutionPath(goalNode):
    """
        Extract a list of nodes walked working back up from the goal node
        to the initialNode
    """
    nodes = list()
    node = goalNode
    while node:
        if node.parent:
            nodes.append(node)
        node = node.parent
    nodes.reverse()
    return nodes
