from state import Board, Piece, find_piece
from move import Move


def gameStateSolved(board):
    """Returns True if the board is in a 'solved' state, False otherwise"""
    isSolved = True
    for row in board.rows:
        for cell in row:
            if cell == -1:
                isSolved = False
                break
    return isSolved


def allMoves(board):
    """
        Return a list of unique moves for all tiles in the current board state
    """
    maxPieceNumber = Board.MasterBrickTile
    for row in board.rows:
        for cell in row:
            maxPieceNumber = max(maxPieceNumber, cell)

    moves = list()
    for pieceNumber in range(Board.MasterBrickTile, maxPieceNumber+1):
        moves = moves + allMovesHelp(board, pieceNumber)

    return moves


def allMovesHelp(board, pieceNumber):
    """
        Given the current state of a board and a piece (integer)
        return a list of all possible moves, or empty list if none possible.
        Moves are returned sorted in the order (if possible):
        'up', 'down', 'left', 'right'
    """
    moves = list()
    piece = find_piece(pieceNumber, board)

    # Find out if there is space above the piece
    moveUpPossible = doesSpaceAboveExist(board, piece)
    if moveUpPossible:
        moves.append(Move(pieceNumber, Move.Up))

    # Find out if there is space below the piece
    moveDownPossible = doesSpaceBelowExist(board, piece)
    if moveDownPossible:
        moves.append(Move(pieceNumber, Move.Down))

    # Find out if there is space to the left of the piece
    moveLeftPossible = doesSpaceExistToLeft(board, piece)
    if moveLeftPossible:
        moves.append(Move(pieceNumber, Move.Left))

    # Find out if there is space to the right of the piece
    moveRightPossible = doesSpaceExistToRight(board, piece)
    if moveRightPossible:
        moves.append(Move(pieceNumber, Move.Right))

    return moves


def doesSpaceAboveExist(board, piece):
    # We need to check the cells directly above the piece,
    # for the entire width of the piece
    # If the cells are all 0, or -1 (and piece is master),
    # there there is space to move there
    searchRowIndex = piece.rowIndex - 1
    if searchRowIndex < 0:
        return False  # Out of bounds

    for colIndex in range(piece.colIndex, (piece.colIndex + piece.width)):
        cell = board.rows[searchRowIndex][colIndex]
        if not __canPieceMoveToCell(piece.number, cell):
            return False
    return True


def doesSpaceBelowExist(board, piece):
    # Similar to doesSpaceExistAbove,
    # but check below the tile (need to keep height in mind)
    searchRowIndex = piece.rowIndex + piece.height
    if searchRowIndex >= board.height:
        return False  # Out of bounds

    for colIndex in range(piece.colIndex, (piece.colIndex + piece.width)):
        cell = board.rows[searchRowIndex][colIndex]
        if not __canPieceMoveToCell(piece.number, cell):
            return False
    return True


def doesSpaceExistToLeft(board, piece):
    # Starting at the row index of piece, check each row for the entire height
    # Search at the piece's colIndex -1 (directly left)
    # If we find an obstacle at any row (i.e. non-zero,
    # non-negative for master) there is an obstacle, so exit w/ False
    searchColIndex = piece.colIndex - 1
    if searchColIndex < 0:
        return False  # Out of bounds

    for rowIndex in range(piece.rowIndex, piece.rowIndex + piece.height):
        cell = board.rows[rowIndex][searchColIndex]
        if not __canPieceMoveToCell(piece.number, cell):
            return False  # Obstacle found
    return True


def doesSpaceExistToRight(board, piece):
    # Starting at the row index of piece, check each row for the entire height
    # Search at the pieces ColIndex + Piece Width (to the right)
    # If we find an obstacle at any row (i.e. non-zero,
    # non-negative for master) there is an obstacle, so exit w/ False
    searchColIndex = piece.colIndex + piece.width
    if searchColIndex >= board.height:
        return False  # Out of bounds

    for rowIndex in range(piece.rowIndex, piece.rowIndex + piece.height):
        cell = board.rows[rowIndex][searchColIndex]
        if not __canPieceMoveToCell(piece.number, cell):
            return False  # Obstacle found
    return True


def __canPieceMoveToCell(pieceNumber, destinationCellContents):
    """
        Return true or false if the pieceNumber can move into the cell
        given the contents of the destination cell.
        (Master Tile can move into '-1' as well as '0')
    """
    isPieceMasterTile = pieceNumber == Board.MasterBrickTile
    isDestinationEmpty = destinationCellContents == Board.EmptyTile

    if isPieceMasterTile:
        return isDestinationEmpty or destinationCellContents == Board.GoalTile
    else:
        return isDestinationEmpty
