import csv
import copy


class Board:
    """An object representing a sliding puzzle board"""

    GoalTile = -1
    EmptyTile = 0
    WallTile = 1
    MasterBrickTile = 2

    def __init__(self, width, height):
        self.__width = width
        self.__height = height
        # Initialize the board with empty tiles
        self.__board = list(list(Board.EmptyTile for column in range(width)) for row in range(height))

    def __hash__(self):
        result = 0
        for col in range(0, self.width):
            for row in range(0, self.height):
                result += self.rows[row][col]

        return result

    @property
    def rows(self):
        return self.__board

    @property
    def width(self):
        return self.__width

    @property
    def height(self):
        return self.__height

    def __eq__(self, other):
        if other is None:
            return False

        if self.height != other.height:
            return False

        if self.width != other.width:
            return False

        equal = True

        for rowIndex in range(self.height):
            for colIndex in range(self.width):
                cellInSelf = self.rows[rowIndex][colIndex]
                cellInOther = other.rows[rowIndex][colIndex]
                if cellInSelf != cellInOther:
                    equal = False
                    break

        return equal

    def __ne__(self, other):
        return not self.__eq__(other)

    def normalizeState(self):
        """"Return a new board in the normalized form"""
        clone = cloneGameState(self)
        nextIndex = Board.MasterBrickTile + 1
        for rowIndex in range(self.height):
            for colIndex in range(self.width):
                if clone.rows[rowIndex][colIndex] == nextIndex:
                    nextIndex = nextIndex + 1
                elif clone.rows[rowIndex][colIndex] > nextIndex:
                    clone.__swapIndex(nextIndex,
                                      clone.rows[rowIndex][colIndex])
                    nextIndex = nextIndex + 1
        return clone

    def __swapIndex(self, indexA, indexB):
        """
            Swap the cells in the board with value 'indexA'
            with the value 'indexB'
        """
        for rowIndex in range(self.height):
            for colIndex in range(self.width):
                if self.rows[rowIndex][colIndex] == indexA:
                    self.rows[rowIndex][colIndex] = indexB
                elif self.rows[rowIndex][colIndex] == indexB:
                    self.rows[rowIndex][colIndex] = indexA

    def __str__(self):
        """Print a Sliding Board State to standard out"""
        result = ''
        result = result + '{0},{1},\n'.format(self.width, self.height)
        for row in self.rows:
            result = result + ','.join([str(cell) for cell in row]) + ',\n'
        return result

    def __repr__(self):
        result = '\n'
        for row in self.rows:
            result = result + ','.join([str(cell) for cell in row]) + '\n'
        return result


class Piece:
    """A class representing a piece on a board."""

    def __init__(self, number, width=0, height=0):
        self.__width = width
        self.__height = height
        self.__number = number
        self.__rowIndex = 0
        self.__colIndex = 0

    @property
    def width(self):
        """The width of a piece"""
        return self.__width

    @width.setter
    def width(self, value):
        self.__width = value

    @property
    def height(self):
        """The width of a piece"""
        return self.__height

    @height.setter
    def height(self, value):
        self.__height = value

    @property
    def number(self):
        return self.__number

    @property
    def rowIndex(self):
        """The row index of the top-left corner"""
        return self.__rowIndex

    @rowIndex.setter
    def rowIndex(self, value):
        self.__rowIndex = value

    @property
    def colIndex(self):
        """The column index of the top-left corner"""
        return self.__colIndex

    @colIndex.setter
    def colIndex(self, value):
        self.__colIndex = value

    def __str__(self):
        return (("number: {0}, rowIndex: {1}, colIndex: {2}, "
                "width: {3}, height: {4}").format(self.number,
                self.rowIndex, self.colIndex,
                self.width, self.height))


def stateEqual(boardA, boardB):
    """Return true if the two boards are in the same state"""
    equal = True

    for rowIndex in range(boardA.height):
        for colIndex in range(boardA.width):
            cellInBoardA = boardA.rows[rowIndex][colIndex]
            cellInBoardB = boardB.rows[rowIndex][colIndex]
            if cellInBoardA != cellInBoardB:
                equal = False
                break

    return equal


def loadGameState(filename):
    """Load a Sliding Board State from Disk"""

    with open(filename, 'rb') as csvfile:
        stateReader = csv.reader(csvfile, delimiter=',', quotechar='|')

        firstRow = stateReader.next()
        board = Board(int(firstRow[0]), int(firstRow[1]))
        board.rows = [[int(cell) for cell in row[0:-1]] for row in stateReader]

    return board


def outputGameState(board):
    """Print a Sliding Board State to standard out"""
    print board


def cloneGameState(board):
    """Returns a deep clone of the given board"""
    newBoard = Board(board.width, board.height)
    newBoard.rows = copy.deepcopy(board.rows)
    return newBoard


def find_piece(piece_number, board):
    piece = Piece(piece_number)

    # Find the first location (top,left)
    for rowIndex in range(len(board.rows)):
        for colIndex in range(len(board.rows[rowIndex])):
            cell = board.rows[rowIndex][colIndex]
            if cell == piece_number:
                # we found the first occurence of the pieceNumber,
                # save it's location
                if piece.rowIndex == 0 and piece.colIndex == 0:
                    piece.rowIndex = rowIndex
                    piece.colIndex = colIndex
                    break

    # We know pieces are rectangular, so at the given rowIndex,
    # find the piece width
    for cell in board.rows[piece.rowIndex]:
        if cell == piece.number:
            piece.width = piece.width + 1

    # Given the piece colIndex, search all rows to find the height
    for row in board.rows:
        cell = row[piece.colIndex]
        if cell == piece.number:
            piece.height = piece.height + 1

    if piece.height == 0 and piece.width == 0:
        return None

    return piece
