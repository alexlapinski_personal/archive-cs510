#!/bin/bash
#
# A simple bash script used to create the text file output for the HW submission
# This runs the othello.runner module with various inputs / algorithms as described in README.md
#

rm ./games.csv

# Run random against each other for baseline
echo "Running 20 iterations of Random vs Random"
#for i in `seq 1 20`;
#do
  #python -m othello.runner random random
#done
echo

depths=(1 2 3 4)
# Run MinMax 10 games w/ increasing depth
echo "Running 10 iterations of Random vs Minimax"
#for j in ${depths[@]};
#do
#  for i in `seq 1 10`;
#  do
    #python -m othello.runner random minimax:${j}
#  done
#done
echo

# Minimax again, but swap player positions
echo "Running 10 iterations of Minimax vs Random"
#for j in ${depths[@]};
#do
#  for i in `seq 1 10`;
#  do
#    #python -m othello.runner minimax:${j} random
#  done
#done


# Run MonteCarlo against random
iterations=(1 100 1000 10000)
echo "Running 10 iterations of Random vs MCTS"
#for j in ${iterations[@]};
#do
#  for i in `seq 1 10`;
#  do
#    #python -m othello.runner random montecarlo:${j}
#  done
#done

# MonteCarlo again, swapping players
echo "Running 10 iterations of MCTS vs Random"
#for j in ${iterations[@]};
#do
#  for i in `seq 1 10`;
#  do
#    #python -m othello.runner montecarlo:${j} random
#  done
#done

# MonteCarlo vs minimax
echo "Running 10 iterations of MCTS vs Minimax"
for j in ${iterations[@]};
do
  for k in ${depths[@]};
  do
    for i in `seq 1 10`;
    do
      python -m othello.runner montecarlo:${j} minimax:${k}
    done
  done
done

# Minimax vs Montecarlo
echo "Running 10 iterations of Minimax vs MCTS"
for j in ${iterations[@]};
do
  for k in ${depths[@]};
  do
    for i in `seq 1 10`;
    do
      python -m othello.runner minimax:${k} montecarlo:${j}
    done
  done
done
