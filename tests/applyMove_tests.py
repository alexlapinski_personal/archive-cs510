import unittest
from state import Board
from move import Move
from mutator import applyMove, applyMoveCloning


class ApplyMoveTests(unittest.TestCase):

        def test_applyMove(self):
                board = Board(4, 4)
                board.rows[0] = [1, 1, 1, 1]
                board.rows[1] = [-1, 0, 3, 1]
                board.rows[2] = [1, 4, 2, 1]
                board.rows[3] = [1, 1, 1, 1]

                move = Move(3, Move.Left)

                applyMove(board, move)

                expectedBoard = Board(4, 4)
                expectedBoard.rows[0] = [1, 1, 1, 1]
                expectedBoard.rows[1] = [-1, 3, 0, 1]
                expectedBoard.rows[2] = [1, 4, 2, 1]
                expectedBoard.rows[3] = [1, 1, 1, 1]

                for r in range(0, board.height):
                        self.assertEqual(board.rows[r], expectedBoard.rows[r])

        def test_applyMoveCloning(self):
                board = Board(4, 4)
                board.rows[0] = [1, 1, 1, 1]
                board.rows[1] = [-1, 0, 3, 1]
                board.rows[2] = [1, 4, 2, 1]
                board.rows[3] = [1, 1, 1, 1]

                move = Move(3, Move.Left)

                clone = applyMoveCloning(board, move)

                expectedBoard = Board(4, 4)
                expectedBoard.rows[0] = [1, 1, 1, 1]
                expectedBoard.rows[1] = [-1, 3, 0, 1]
                expectedBoard.rows[2] = [1, 4, 2, 1]
                expectedBoard.rows[3] = [1, 1, 1, 1]

                for r in range(0, board.height):
                        self.assertEqual(clone.rows[r], expectedBoard.rows[r])

                board.rows[0][1] = -1
                self.assertNotEqual(board.rows[0][1], clone.rows[0][1])
