import unittest
from state import Board


class TestBoard(unittest.TestCase):

    def test_rows_init_empty(self):
        board = Board(0, 0)
        self.assertEqual(len(board.rows), 0)
        self.assertEqual(board.rows, [])

    def test_rows_init(self):
        for w in range(1, 3):
            for h in range(1, 3):
                board = Board(w, h)
                self.assertEqual(len(board.rows), h)

                for i in range(0, h):
                    self.assertEqual(len(board.rows[i]), w)
                    for j in range(0, w):
                        self.assertEqual(board.rows[i][j], 0)

    def test_rows_set(self):
        board = Board(1, 1)
        board.rows[0][0] = 10
        self.assertEqual(board.rows[0][0], 10)

    def test_width(self):
        for i in range(0, 3):
            board = Board(i, 1)
            self.assertEqual(board.width, i)
            self.assertEqual(board.height, 1)

    def test_height(self):
        for i in range(0, 3):
            board = Board(1, i)
            self.assertEqual(board.height, i)
            self.assertEqual(board.width, 1)

    def test_normalizeState_simple(self):
        # Given
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 4, 3, 1]
        board.rows[2] = [-1, 0, 2, 1]
        board.rows[3] = [1, 1, 1, 1]

        # When
        clone = board.normalizeState()

        # Then
        self.assertEqual(clone.rows[0], [1, 1, 1, 1])
        self.assertEqual(clone.rows[1], [1, 3, 4, 1])
        self.assertEqual(clone.rows[2], [-1, 0, 2, 1])
        self.assertEqual(clone.rows[3], [1, 1, 1, 1])

    def test_normalizeState_complex(self):
        # Given
        board = Board(6, 6)
        board.rows[0] = [1, 1, 1, 1, 1, 1]
        board.rows[1] = [1, 4, 4, 3, 5, 1]
        board.rows[2] = [-1, 4, 4, 3, 0, 1]
        board.rows[3] = [-1, 7, 7, 0, 2, 1]
        board.rows[4] = [1, 6, 0, 0, 2, 1]
        board.rows[5] = [1, 1, 1, 1, 1, 1]

        # When
        clone = board.normalizeState()

        # Then
        self.assertEqual(clone.rows[0], [1, 1, 1, 1, 1, 1])
        self.assertEqual(clone.rows[1], [1, 3, 3, 4, 5, 1])
        self.assertEqual(clone.rows[2], [-1, 3, 3, 4, 0, 1])
        self.assertEqual(clone.rows[3], [-1, 6, 6, 0, 2, 1])
        self.assertEqual(clone.rows[4], [1, 7, 0, 0, 2, 1])
        self.assertEqual(clone.rows[5], [1, 1, 1, 1, 1, 1])

    def test_str(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 0, 2, 1]
        board.rows[2] = [-1, 3, 0, 1]
        board.rows[3] = [1, 1, 1, 1]

        expected = ("4,4,\n"
                    "1,1,1,1,\n"
                    "1,0,2,1,\n"
                    "-1,3,0,1,\n"
                    "1,1,1,1,\n")

        self.assertEqual(str(board), expected)

    def test_repr(self):
        board = Board(4, 4)

        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 0, 2, 1]
        board.rows[2] = [-1, 3, 0, 1]
        board.rows[3] = [1, 1, 1, 1]

        expected = ("\n"
                    "1,1,1,1\n"
                    "1,0,2,1\n"
                    "-1,3,0,1\n"
                    "1,1,1,1\n")

        self.assertEqual(repr(board), expected)

