import unittest
from state import Board
from heuristics import manhattan_distance_cust


class CustomHeuristicTests(unittest.TestCase):

    def testLevel0(self):
        board = Board(5, 4)
        board.rows[0] = [1, -1, -1, 1, 1]
        board.rows[1] = [1, 0, 3, 4, 1]
        board.rows[2] = [1, 0, 2, 2, 1]
        board.rows[3] = [1, 1, 1, 1, 1]

        h = manhattan_distance_cust(board)

        self.assertEqual(h, 3)

    def testLevel0_move1(self):
        board = Board(5, 4)
        board.rows[0] = [1, -1, -1, 1, 1]
        board.rows[1] = [1, 0, 3, 4, 1]
        board.rows[2] = [1, 2, 2, 0, 1]
        board.rows[3] = [1, 1, 1, 1, 1]

        h = manhattan_distance_cust(board)

        self.assertEqual(h, 2)

    def testLevel0_move2(self):
        board = Board(5, 4)
        board.rows[0] = [1, -1, -1, 1, 1]
        board.rows[1] = [1, 0, 3, 0, 1]
        board.rows[2] = [1, 2, 2, 4, 1]
        board.rows[3] = [1, 1, 1, 1, 1]

        h = manhattan_distance_cust(board)

        self.assertEqual(h, 2)

    def testLevel0_move3(self):
        board = Board(5, 4)
        board.rows[0] = [1, -1, -1, 1, 1]
        board.rows[1] = [1, 0, 0, 3, 1]
        board.rows[2] = [1, 2, 2, 4, 1]
        board.rows[3] = [1, 1, 1, 1, 1]

        h = manhattan_distance_cust(board)

        self.assertEqual(h, 2)

    def testLevel0Reversed(self):
        board = Board(5, 4)
        board.rows[0] = [1, 1, 1, 1, 1]
        board.rows[1] = [1, 0, 2, 2, 1]
        board.rows[2] = [1, 0, 3, 4, 1]
        board.rows[3] = [1, -1, -1, 1, 1]

        h = manhattan_distance_cust(board)

        self.assertEqual(h, 3)

    def testLevel0Left(self):
        board = Board(5, 4)
        board.rows[0] = [1, 1, 1, 1, 1]
        board.rows[1] = [-1, 0, 3, 2, 1]
        board.rows[2] = [-1, 0, 0, 2, 1]
        board.rows[3] = [1, 1, 1, 1, 1]

        h = manhattan_distance_cust(board)

        self.assertEqual(h, 4)
