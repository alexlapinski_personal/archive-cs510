import unittest
from state import Board
from heuristics import manhattan_distance


class ManhattanTests(unittest.TestCase):

    def test_null(self):
        distance = manhattan_distance(None)

        self.assertEqual(distance, 0)

    def test_zero_space(self):
        board = Board(0, 0)

        distance = manhattan_distance(board)

        self.assertEqual(distance, 0)

    def test_no_master_brick(self):
        board = Board(3, 3)
        board.rows[0] = [1, 1, 1]
        board.rows[1] = [1, 0, 1]
        board.rows[2] = [1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 0)

    def test_no_goal_tile(self):
        board = Board(3, 3)
        board.rows[0] = [1, 1, 1]
        board.rows[1] = [1, 2, 1]
        board.rows[2] = [1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 0)

    def test_distance_one_left(self):
        board = Board(3, 3)
        board.rows[0] = [1, 1, 1]
        board.rows[1] = [-1, 2, 1]
        board.rows[2] = [1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_one_right(self):
        board = Board(3, 3)
        board.rows[0] = [1, 1, 1]
        board.rows[1] = [1, 2, -1]
        board.rows[2] = [1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_one_up(self):
        board = Board(3, 3)
        board.rows[0] = [1, -1, 1]
        board.rows[1] = [1, 2, 1]
        board.rows[2] = [1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_one_down(self):
        board = Board(3, 3)
        board.rows[0] = [1, 1, 1]
        board.rows[1] = [1, 2, 1]
        board.rows[2] = [1, -1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_down_left(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 0, 2, 1]
        board.rows[2] = [-1, 0, 0, 1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 3)

    def test_distance_down_right(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 2, 0, 1]
        board.rows[2] = [1, 0, 0, -1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 3)

    def test_distance_up_left(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [-1, 0, 0, 1]
        board.rows[2] = [1, 0, 2, 1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 3)

    def test_distance_up_right(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 0, 0, -1]
        board.rows[2] = [1, 2, 0, 1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 3)

    def test_distance_wide_right(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 0, 0, 1]
        board.rows[2] = [1, 2, 2, -1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_wide_left(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 0, 0, 1]
        board.rows[2] = [-1, 2, 2, 1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_wide_left_up(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [-1, 0, 0, 1]
        board.rows[2] = [1, 2, 2, 1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 2)

    def test_distance_wide_right_up(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 0, 0, -1]
        board.rows[2] = [1, 2, 2, 1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 2)

    def test_distance_wide_up(self):
        board = Board(4, 4)
        board.rows[0] = [1, -1, -1, 1]
        board.rows[1] = [1, 0, 0, 1]
        board.rows[2] = [1, 2, 2, 1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 2)

    def test_distance_wide_down(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 0, 0, 1]
        board.rows[2] = [1, 2, 2, 1]
        board.rows[3] = [1, -1, -1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_tall_down(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 2, 0, 1]
        board.rows[2] = [1, 2, 0, 1]
        board.rows[3] = [1, -1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_tall_up(self):
        board = Board(4, 4)
        board.rows[0] = [1, -1, 1, 1]
        board.rows[1] = [1, 2, 0, 1]
        board.rows[2] = [1, 2, 0, 1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_tall_left(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [-1, 2, 0, 1]
        board.rows[2] = [-1, 2, 0, 1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 1)

    def test_distance_tall_right(self):
        board = Board(4, 4)
        board.rows[0] = [1, 1, 1, 1]
        board.rows[1] = [1, 2, 0, -1]
        board.rows[2] = [1, 2, 0, -1]
        board.rows[3] = [1, 1, 1, 1]

        distance = manhattan_distance(board)

        self.assertEqual(distance, 2)
