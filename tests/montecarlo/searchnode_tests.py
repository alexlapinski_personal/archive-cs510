import unittest
from othello.state import OthelloState
from othello.montecarlo_player import SearchNode


class SearchNodeTests(unittest.TestCase):

    def test_init(self):
        state = OthelloState(8)
        node = SearchNode(state)
        self.assertIsNotNone(node.state)
        self.assertIsNone(node.action)
        self.assertIsNone(node.parent)
        self.assertEqual(len(node.children), 0)
        self.assertEqual(node.times_visited, 0)
        self.assertEqual(node.average_score, 0)

    def test_add_child(self):
        state = OthelloState(8)
        node = SearchNode(state)
        move = state.generate_moves().pop(0)

        self.assertIsNotNone(move)
        child = node.add_child(move)

        self.assertEqual(child.parent, node)
        self.assertIn(child, node.children)
        self.assertEqual(child.action, move)

    def test_moves_not_in_children_should_be_all(self):
        state = OthelloState(8)

        node = SearchNode(state)
        expected_moves = state.generate_moves()
        actual_moves = node.moves_not_in_children()

        for move in expected_moves:
            self.assertIn(move, actual_moves)

        for move in actual_moves:
            self.assertIn(move, expected_moves)

    def test_moves_not_in_children_should_be_all_but_one(self):
        state = OthelloState(8)

        node = SearchNode(state)
        expected_moves = state.generate_moves()
        move_to_add = expected_moves.pop(0)

        node.add_child(move_to_add)

        actual_moves = node.moves_not_in_children()

        for move in expected_moves:
            self.assertIn(move, actual_moves)

        for move in actual_moves:
            self.assertIn(move, expected_moves)

    def test_moves_not_in_children_should_be_none(self):
        state = OthelloState(8)

        node = SearchNode(state)
        moves = state.generate_moves()

        for move in moves:
            node.add_child(move)

        actual_moves = node.moves_not_in_children()
        self.assertEqual(len(actual_moves), 0)
