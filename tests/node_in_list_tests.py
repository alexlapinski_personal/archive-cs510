import unittest
from state import Board
from search.model import Node
from searchUtils import isNodeInList


class NodeInListTests(unittest.TestCase):

    def setUp(self):
        self.board1 = Board(5, 4)
        self.board1.rows[0] = [1, -1, -1, 1, 1]
        self.board1.rows[1] = [1, 0, 3, 4, 1]
        self.board1.rows[2] = [1, 0, 2, 2, 1]
        self.board1.rows[3] = [1, 1, 1, 1, 1]

        # Board2 after normalizing, equals board1
        self.board2 = Board(5, 4)
        self.board2.rows[0] = [1, -1, -1, 1, 1]
        self.board2.rows[1] = [1, 0, 4, 3, 1]
        self.board2.rows[2] = [1, 0, 2, 2, 1]
        self.board2.rows[3] = [1, 1, 1, 1, 1]

        self.board3 = Board(5, 4)
        self.board3.rows[0] = [1, -1, -1, 1, 1]
        self.board3.rows[1] = [1, 0, 3, 4, 1]
        self.board3.rows[2] = [1, 2, 2, 0, 1]
        self.board3.rows[3] = [1, 1, 1, 1, 1]

        self.board4 = Board(5, 4)
        self.board4.rows[0] = [1, -1, -1, 1, 1]
        self.board4.rows[1] = [1, 3, 0, 4, 1]
        self.board4.rows[2] = [1, 2, 2, 0, 1]
        self.board4.rows[3] = [1, 1, 1, 1, 1]

    def test_emptyList(self):
        l = []

        isInList = isNodeInList(Node(self.board1), l)
        self.assertFalse(isInList)

        isInList = isNodeInList(Node(self.board2), l)
        self.assertFalse(isInList)

        isInList = isNodeInList(Node(self.board3), l)
        self.assertFalse(isInList)

        isInList = isNodeInList(Node(self.board4), l)
        self.assertFalse(isInList)

    def test_oneItemInList(self):
        l = []
        l.append(Node(self.board1))

        isInList = isNodeInList(Node(self.board1), l)
        self.assertTrue(isInList)

        isInList = isNodeInList(Node(self.board3), l)
        self.assertFalse(isInList)

    def test_twoItemsInList(self):
        l = []
        l.append(Node(self.board1))
        l.append(Node(self.board3))

        isInList = isNodeInList(Node(self.board1), l)
        self.assertTrue(isInList)

        isInList = isNodeInList(Node(self.board3), l)
        self.assertTrue(isInList)

        isInList = isNodeInList(Node(self.board4), l)
        self.assertFalse(isInList)

    def test_oneItemNeedsNormalization(self):
        l = []
        l.append(Node(self.board1))

        # board1 is equivilent to board2 after normalization
        isInList = isNodeInList(Node(self.board2), l)
        self.assertTrue(isInList)

        isInList = isNodeInList(Node(self.board3), l)
        self.assertFalse(isInList)

