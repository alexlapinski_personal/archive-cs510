import unittest
from othello.move import OthelloMove
from othello.state import OthelloState


class GenerateMovesTests(unittest.TestCase):

    def test_generate_moves_initialstate_player0(self):
        state = OthelloState(8)
        moves = state.generate_moves(0)

        expectedMoves = []
        expectedMoves.append(OthelloMove(0, 2, 4))
        expectedMoves.append(OthelloMove(0, 3, 5))
        expectedMoves.append(OthelloMove(0, 4, 2))
        expectedMoves.append(OthelloMove(0, 5, 3))

        self.__assert_moves(expectedMoves, moves)

    def test_generate_moves_initialstate_player1(self):
        state = OthelloState(8)
        moves = state.generate_moves(1)

        expectedMoves = []
        expectedMoves.append(OthelloMove(1, 2, 3))
        expectedMoves.append(OthelloMove(1, 3, 2))
        expectedMoves.append(OthelloMove(1, 4, 5))
        expectedMoves.append(OthelloMove(1, 5, 4))

        self.__assert_moves(expectedMoves, moves)

    def test_generate_moves_step1_player0(self):
        state = OthelloState(8)
        state.apply_move(OthelloMove(0, 2, 4))

        moves = state.generate_moves(0)

        expectedMoves = []
        expectedMoves.append(OthelloMove(0, 4, 2))
        expectedMoves.append(OthelloMove(0, 5, 2))
        expectedMoves.append(OthelloMove(0, 5, 3))

        self.__assert_moves(expectedMoves, moves)

    def test_generate_moves_step1_player1(self):
        state = OthelloState(8)
        state.apply_move(OthelloMove(1, 2, 3))

        moves = state.generate_moves(1)

        expectedMoves = []
        expectedMoves.append(OthelloMove(1, 4, 5))
        expectedMoves.append(OthelloMove(1, 5, 4))
        expectedMoves.append(OthelloMove(1, 5, 5))

        self.__assert_moves(moves, expectedMoves)

    def test_generate_moves_step2_player0(self):
        state = OthelloState(8)
        state.apply_move(OthelloMove(0, 4, 2))

        moves = state.generate_moves(0)

        expectedMoves = []
        expectedMoves.append(OthelloMove(0, 2, 4))
        expectedMoves.append(OthelloMove(0, 2, 5))
        expectedMoves.append(OthelloMove(0, 3, 5))

        self.__assert_moves(expectedMoves, moves)

    def test_generate_moves_step2_player1(self):
        state = OthelloState(8)
        state.apply_move(OthelloMove(1, 3, 2))

        moves = state.generate_moves(1)

        expectedMoves = []
        expectedMoves.append(OthelloMove(1, 4, 5))
        expectedMoves.append(OthelloMove(1, 5, 4))
        expectedMoves.append(OthelloMove(1, 5, 5))

        self.__assert_moves(moves, expectedMoves)

    def test_generate_moves_step3_player0(self):
        state = OthelloState(8)
        state.apply_move(OthelloMove(0, 3, 5))

        moves = state.generate_moves(0)
        expectedMoves = []
        expectedMoves.append(OthelloMove(0, 4, 2))
        expectedMoves.append(OthelloMove(0, 5, 2))
        expectedMoves.append(OthelloMove(0, 5, 3))

        self.__assert_moves(expectedMoves, moves)

    def test_generate_moves_step3_player1(self):
        state = OthelloState(8)
        state.apply_move(OthelloMove(1, 4, 5))

        moves = state.generate_moves(1)

        expectedMoves = []
        expectedMoves.append(OthelloMove(1, 2, 2))
        expectedMoves.append(OthelloMove(1, 2, 3))
        expectedMoves.append(OthelloMove(1, 3, 2))

        self.__assert_moves(moves, expectedMoves)



    def __assert_moves(self, expectedMoves, actualMoves):
        for i in xrange(len(actualMoves)):
            expectedMove = expectedMoves[i]
            actualMove = actualMoves[i]

            self.assertEqual(actualMove.player, expectedMove.player)
            self.assertEqual(actualMove.x, expectedMove.x)
            self.assertEqual(actualMove.y, expectedMove.y)
