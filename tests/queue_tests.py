import unittest
from state import Board, cloneGameState
from search.model import Node
from search.queue import PriorityQueue


class PriorityQueueTests(unittest.TestCase):

    def setUp(self):
        self.board1 = Board(5, 4)
        self.board1.rows[0] = [1, -1, -1, 1, 1]
        self.board1.rows[1] = [1, 0, 3, 4, 1]
        self.board1.rows[2] = [1, 0, 2, 2, 1]
        self.board1.rows[3] = [1, 1, 1, 1, 1]

        self.board2 = Board(5, 4)
        self.board2.rows[0] = [1, -1, -1, 1, 1]
        self.board2.rows[1] = [1, 0, 3, 4, 1]
        self.board2.rows[2] = [1, 2, 2, 0, 1]
        self.board2.rows[3] = [1, 1, 1, 1, 1]

        self.board3 = Board(5, 4)
        self.board3.rows[0] = [1, -1, -1, 1, 1]
        self.board3.rows[1] = [1, 3, 0, 4, 1]
        self.board3.rows[2] = [1, 2, 2, 0, 1]
        self.board3.rows[3] = [1, 1, 1, 1, 1]

    def test_addOne(self):
        queue = PriorityQueue()
        node = Node(None)
        node.g = 1
        node.h = 0

        queue.push(node)

        result = queue.pop()

        self.assertEqual(result.g, node.g)
        self.assertEqual(result.h, node.h)

    def test_addTwo_hEqual(self):
        queue = PriorityQueue()
        node1 = Node(None)
        node1.g = 1
        node1.h = 1

        node2 = Node(None)
        node2.g = 2
        node2.h = 1

        queue.push(node1)
        queue.push(node2)

        result = queue.pop()

        self.assertEqual(result.g, node1.g)
        self.assertEqual(result.h, node1.h)

        queue2 = PriorityQueue()
        queue2.push(node2)
        queue2.push(node1)

        result2 = queue2.pop()

        self.assertEqual(result2.g, node1.g)
        self.assertEqual(result2.h, node1.h)

    def test_addTwo_gEqual(self):
        queue = PriorityQueue()
        node1 = Node(None)
        node1.g = 1
        node1.h = 1

        node2 = Node(None)
        node2.g = 1
        node2.h = 2

        queue.push(node1)
        queue.push(node2)

        result = queue.pop()

        self.assertEqual(result.g, node1.g)
        self.assertEqual(result.h, node1.h)

        queue2 = PriorityQueue()
        queue2.push(node2)
        queue2.push(node1)

        result2 = queue2.pop()

        self.assertEqual(result2.g, node1.g)
        self.assertEqual(result2.h, node1.h)

    def test_nonZero_empty(self):
        queue = PriorityQueue()

        self.assertFalse(queue)

    def test_nonZero_sizeOne(self):
        queue = PriorityQueue()
        queue.push(Node(Board(1, 1)))

        self.assertTrue(queue)

    def test_nonZero_sizeOne_pop(self):
        queue = PriorityQueue()
        queue.push(Node(Board(1, 1)))

        self.assertTrue(queue)

        queue.pop()

        self.assertFalse(queue)

    def test_same_priority(self):
        # When two nodes of the same priority were added
        # the first one added should be popped first

        queue = PriorityQueue()
        node1 = Node(cloneGameState(self.board1))
        node1.g = 0
        node1.h = 3

        node2 = Node(cloneGameState(self.board2))
        node2.g = 1
        node2.h = 3

        node3 = Node(cloneGameState(self.board3))
        node3.g = 1
        node3.h = 3

        queue.push(node1)
        queue.push(node2)  # we expect to get node 2 back
        queue.push(node3)

        pop1 = queue.pop()  # this should be board1
        self.assertEqual(pop1.state, self.board1)

        pop2 = queue.pop()  # this should be board2
        self.assertEqual(pop2.state, self.board2)

        pop3 = queue.pop()  # this should be board 3
        self.assertEqual(pop3.state, self.board3)
