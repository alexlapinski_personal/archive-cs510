import unittest
from state import Board, stateEqual, cloneGameState


class StateTests(unittest.TestCase):

    def test_stateEqual_notEqual(self):
        boardA = Board(4, 4)
        boardA.rows[0] = [1, 1, 1, 1]
        boardA.rows[1] = [-1, 0, 3, 0]
        boardA.rows[2] = [1, 4, 2, 0]
        boardA.rows[3] = [1, 1, 1, 1]

        boardB = Board(4, 4)
        boardB.rows[0] = [1, 1, 1, 1]
        boardB.rows[1] = [-1, 0, 4, 0]
        boardB.rows[2] = [1, 3, 2, 0]
        boardB.rows[3] = [1, 1, 1, 1]

        self.assertFalse(stateEqual(boardA, boardB))

    def test_stateEqual_notEqual_case2(self):
        boardA = Board(4, 4)
        boardA.rows[0] = [1, 1, 1, 1]
        boardA.rows[1] = [-1, 0, 3, 1]
        boardA.rows[2] = [1, 4, 2, 1]
        boardA.rows[3] = [1, 1, 1, 1]

        boardB = Board(4, 4)
        boardB.rows[0] = [-1, 1, 1, 1]
        boardB.rows[1] = [1, 0, 4, 1]
        boardB.rows[2] = [1, 3, 2, 1]
        boardB.rows[3] = [1, 1, 1, 1]

        self.assertFalse(stateEqual(boardA, boardB))

    def test_stateEqual_notEqual_case3(self):
        boardA = Board(4, 4)
        boardA.rows[0] = [1, 1, 1, 1]
        boardA.rows[1] = [-1, 0, 3, 1]
        boardA.rows[2] = [1, 4, 2, 1]
        boardA.rows[3] = [1, 1, 1, 1]

        boardB = Board(4, 4)
        boardB.rows[0] = [1, 1, 1, 1]
        boardB.rows[1] = [-1, 2, 4, 1]
        boardB.rows[2] = [1, 3, 0, 1]
        boardB.rows[3] = [1, 1, 1, 1]

        self.assertFalse(stateEqual(boardA, boardB))

    def test_stateEqual_areEqual(self):
        boardA = Board(4, 4)
        boardA.rows[0] = [1, 1, 1, 1]
        boardA.rows[1] = [-1, 0, 3, 1]
        boardA.rows[2] = [1, 4, 2, 1]
        boardA.rows[3] = [1, 1, 1, 1]

        boardB = Board(4, 4)
        boardB.rows[0] = [1, 1, 1, 1]
        boardB.rows[1] = [-1, 0, 3, 1]
        boardB.rows[2] = [1, 4, 2, 1]
        boardB.rows[3] = [1, 1, 1, 1]

        self.assertTrue(stateEqual(boardA, boardB))

    def test_cloneGameState(self):
        boardA = Board(1, 1)
        self.assertEqual(boardA.rows[0][0], 0)

        clone = cloneGameState(boardA)
        self.assertEqual(boardA.width, clone.width)
        self.assertEqual(boardA.height, clone.height)
        self.assertEqual(boardA.rows[0][0], clone.rows[0][0])

        clone.rows[0][0] = 1
        self.assertNotEqual(boardA.rows[0][0], clone.rows[0][0])

